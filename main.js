document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('task-form');
    const taskList = document.getElementById('task-list');
    const taskInput = document.getElementById('task-input');

    // Load tasks from local storage
    loadTasks();

    // Add task
    form.addEventListener('submit', addTask);

    // Remove task
    taskList.addEventListener('click', removeTask);

    function addTask(e) {
        e.preventDefault();

        if (taskInput.value === '') {
            alert('Add a task');
            return;
        }

        // Create new task element
        const li = document.createElement('li');
        li.className = 'border-b border-gray-200 py-2 flex justify-between items-center';
        li.appendChild(document.createTextNode(taskInput.value));

        const deleteBtn = document.createElement('button');
        deleteBtn.className = 'bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline';
        deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';
        li.appendChild(deleteBtn);

        taskList.appendChild(li);

        // Store in local storage
        storeTaskInLocalStorage(taskInput.value);

        // Clear input
        taskInput.value = '';
    }

    function removeTask(e) {
        if (e.target.parentElement.classList.contains('bg-red-500')) {
            if (confirm('Are You Sure?')) {
                e.target.parentElement.parentElement.remove();

                // Remove from local storage
                removeTaskFromLocalStorage(e.target.parentElement.parentElement);
            }
        }
    }

    function loadTasks() {
        let tasks;
        if (localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.forEach(function(task) {
            const li = document.createElement('li');
            li.className = 'border-b border-gray-200 py-2 flex justify-between items-center';
            li.appendChild(document.createTextNode(task));

            const deleteBtn = document.createElement('button');
            deleteBtn.className = 'bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline';
            deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';
            li.appendChild(deleteBtn);

            taskList.appendChild(li);
        });
    }

    function storeTaskInLocalStorage(task) {
        let tasks;
        if (localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.push(task);

        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    function removeTaskFromLocalStorage(taskItem) {
        let tasks;
        if (localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.forEach(function(task, index) {
            if (taskItem.textContent === task) {
                tasks.splice(index, 1);
            }
        });

        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
});
